
# Learning new tools in IT

## General

Great for starters cover most of the new tools and methods related to IT

* https://www.youtube.com/@TechWorldwithNana
* https://www.pluralsight.com/
* https://learn.acloud.guru
* https://www.oreilly.com/
* https://www.manning.com/
* https://kodekloud.com


## Amazon (AWS)
https://www.aboutamazon.com/aws/news/aws-cloud-institute-virtual-training-for-cloud-developers


## CyberSecurity

Starting in cyber security great site to learn
https://tryhackme.com/

## Python

* https://www.udemy.com/course/complete-python-bootcamp/
* https://www.codecademy.com/learn/learn-python-3
* https://www.edx.org/course/building-modern-python-applications-on-aws?index=product&queryID=430261afc219e65c47e94cf0bf0f6c2b&position=3&v=2&linked_from=autocomplete&c=autocomplete

## Roadmap

* https://roadmap.sh/
* https://github.com/Developer-Y/cs-video-courses


## Hosting a small site

* https://tiiny.host
* https://www.virtualinfra.online/post/cloudflare-pages/
* https://www.virtualinfra.online/post/thisrunsongitlabpages/

## Certification

* [Kubernetes and Linux Foundation Labs](https://killer.sh)


## Labs

* [A lot of labs](https://killercoda.com/)
* [K8s Labs](https://labs.play-with-k8s.com/)


## Networking

* [Isovalent labs](https://isovalent.com/resource-library/labs/)