# Links for living in The Netherlands

## Food & Otras yerbas

* <https://www.bol.com/>
* <https://www.pampadrugstore.net/>
* <https://saboresdelplata.es/>
* <https://www.laboca.eu/>
* <https://www.uruguayeces.com/>
* <https://www.ladespensa.nl/>
* <https://beefensteak.nl/>
* <https://www.bbquality.nl/> --> Mollejas, matambre de cerdo, cerdo entero !!
* <https://www.deeleenkoe.be/nl>
* <https://barriolatino.nl/>
* <http://amazingoriental.com/>
* <https://warnsveld.slagerijgelderblom.nl/rundvlees> --> Vaca
* <http://www.hoeveslagerijleeuwenhorn.nl/> --> Carne
* <https://beimerpremium.nl> --> Cheap and amazing meat !!
* <https://chateaubriand.nl> --> Molleja, riñon !!
* <https://latinafy.com>

## Kids Places

* <https://www.efteling.com/nl/park/attractiepark-in-nederland> ( first big park in EU)
* <https://dierenparkamersfoort.nl/> (like a zoo)

## Nature Places

* <https://www.zwemwater.nl/> ( status of water, nice to know if it is possible to swim or fishing or camping )
* <https://keukenhof.nl/en/>
* <https://www.hetvondelpark.net/>
* <https://www.natuurmonumenten.nl/natuurgebieden/nationaal-park-loonse-en-drunense-duinen> ( natural desert area in NL)
* <https://www.burgerszoo.nl/> (zoo)
* <https://www.exploremaastricht.nl/maastricht-underground>

## Art & Science & History

* <https://www.museum.nl/nl/museumkaart>
* <https://www.nemosciencemuseum.nl/nl/>

## Internet check by Postal Code

* <https://www.internetvergelijk.nl/>
* <https://www.breedbandwinkel.nl/>

## Service Providers

* <https://www.mijnaansluiting.nl/home> (check who is your service provider)

## Internet Providers

* <https://freedom.nl/> (static ipv4 and ipv6, you can configure your own reverse (PTR))
* <https://account.nlziet.nl/> ( cheap NL tv)

## Bitcoin

* <https://bitonic.nl/>

## Politica

* <https://nimd.org/wp-content/uploads/2015/02/Dutch-Political-System.pdf>

## Exchange Coin

*Dont bring any cash!*, but if you do you can change it here
*Banks don't accept cash on any Branch!*

* <https://www.abnamro.nl/>  --> ABN Bank Schipol Airport the best change you can get
* <https://www.pottchange.com/>
* <https://www.gwktravelex.nl/>

If you need to transfer money use:

* <https://transferwise.com/>

## Renting

* <https://www.pararius.com/>
* <https://www.123wonen.nl/>
* <https://www.mvgm.nl/>
* <https://ikwilhuren.nu/>
* <https://housinganywhere.com/> --> Airbnb style but for long rents
* <https://www.grover.com/nl-nl> --> rent assets and more

## Banks

### Normal!?

* <https://www.abnamro.nl/> (1)
* <https://www.ing.nl/>

(1) How to Open a Bank Account (expats)
call 020-343-4002 or <appointmentdesk.internationals@nl.abnamro.com>

* Original passport, or European (EU) Identity Card to verify your details. --> scanned passport in pdf
* Signed employment contract. --> work contract
* Your Financial Identification Number (FIN) of the country in which you are currently liable to pay tax. For example your Burger Service Number (BSN) or  Tax Identification Number (TIN) of the country of prior residence. --> CUIL for Argentinians

### Virtual Banks

* <https://www.bunq.com/>
* <https://n26.com/> --> German, but widely accepted in The Netherlands
* <https://www.revolut.com/> --> UK Bank, with stock and Crypto market and also works with ideal.

### Trade

* <https://bitvavo.com/en> , full crypto legal trading app working with ideal and have staking rewards pool

## Bikes

* <https://bikefair.org> --> 2nd hand verified
* <https://www.mantel.com/>

## Good Prices Services

* <https://www.pricewise.nl/>
* <https://www.independer.nl/>

## Finding a Doctor (GP)

* <https://www.zorgkaartnederland.nl/>
* <https://www.independer.nl/>

## Medicines Equivalences

* <https://www.vademecum.es/>

## Electronics

* <https://Tweakers.nl>
* <https://Conrad.nl>
* <https://Kiwi-electronics.nl>
* <https://SOS-Solutions.nl>
* <https://www.myelectronics.nl> (nice pi rack stuff)
* <https://www.serverkast.com/> (nice racks)
* <https://www.tinytronics.nl/shop/en>
* <https://www.partsnl.nl/> | Parts for home appliances koffie, Freezer, etc

## Car Driver course

* <https://www.rijschooldenk.nl/>
* <https://www.theorieexamen.nl/>

## Security

This site allow you to check the Burglary risk in your postal code

* <https://inbraakbarometer.nl/>

## Salary (How to read the payslip)

* <https://www.iamexpat.nl/career/employment-news/how-read-your-dutch-payslip>

## Noord Brabant

Cosas variadas de Noord Brabant

### Carnval

* <https://www.visitbrabant.com/nl/wat-te-doen/carnaval> ( every 11/11 the first party !)

### Rutas

* [Templos Romanos](https://www.erfgoedshertogenbosch.nl/activiteiten/tempels-langs-de-maas)
* [Linea 1629](https://www.erfgoedshertogenbosch.nl/landing/fietsroute-linie-1629)
* [WWII](https://www.brabantremembers.com/ar-app/?lang=en)
* [Caminata 4 dias](https://www.4daagse.nl/en/)

## CitinzenShip | Inburger

* <https://www.naarnederland.nl/>
* <https://www.inburgeringonline.nl>
* <https://www.inburgeren.nl/examen-doen/oefenen.jsp>

## Desks

* <https://mystorageshop.nl/product-category/desks/>

## Insurance (medical)

* <https://www.oominsurance.com/to-the-netherlands/schengen-visitor-insurance/calculate-premiumapply/>


## Taxi

* <https://www.zorg-taxi.nl/>
* <https://www.humanitas.nl/programmas/begeleiding-artsenbezoek/>
