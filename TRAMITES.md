# Argentina

### Pasos para legalizar Partidas de Nacimiento o Matrimonio para Países Bajos:

1. Legalizacion Ministerio del Interior:​ Esta es la validación a nivel nacional de
tu documento o partida. Si sos de CABA este paso saltealo.

   a. Consegui tu documento original o digital (las nuevas partidas de nac por
ejemplo)

   b. Para sacar turno, elegís fecha y horario ​ aca​ . Te anotas con tus datos o quien
vaya a hacer el trámite por vos. Si va otra persona, dale una fotocopia de tu
DNI para que lleve.

   b. Con tu DNI o el de la persona que vaya hay que ir a: Av. Leandro N. Alem 150
1° en el dia y horario que elegiste. Si sos del interior del pais, podes mandar
todo por correo. Más info ​ aca​ .

   d. Llevas los documentos y pedis legalizar. Es gratis y te lo hacen en el momento.

2. Primera apostilla:​ Esta es la validación a nivel Internacional de que tu
documento ​ en español ​ es válido en todo el mundo.

   a. Con el documento legalizado en el Ministerio del Interior, vas a Cancillería, se
saca turno ​ aca​ .

   b. Vas con DNI a: Esmeralda 1214, C.A.B.A. C1007ABR. Si haces el tramite antes
de las 12:30pm te lo devuelven en el dia el papel asi no tenes que volver, sino
al otro día esta listo. Cuesta $300 pesos.

   c. Si sos del interior podes hacer la Apostilla en cualquier colegio de escribanos.

3. Traducción oficial:​ Esta es la traducción oficial de tu documento con la
validación internacional.

   a. Con los papeles legalizados por el Ministerio del Interior, y luego apostillados
por Cancillería, se lo dejas al traductor público de tu elección.

   b. Los traduce y los hace validar por el colegio de traductores. Todo el proceso
tarda unas 72hs. Cuesta algo de $1200 pesos por carilla aprox. + $340 de
validación/legalización en el colegio de traductores.

4. Apostilla Internacional:​ Esta es la validación INTERNACIONAL OFICIAL de
todo, lo que está en idioma original y lo traducido. TODO.

   a. Con el documento legalizado, apostillado y traducido, se saca turno ​ aca
nuevamente.

   b. Vas con DNI a: Esmeralda 1214, C.A.B.A. C1007ABR. Si haces el tramite antes
de las 12pm te lo devuelven en el dia el papel asi no tenes que volver. Cuesta
$300 pesos.

   c. Si sos del interior podes hacer la Apostilla en cualquier colegio de escribanos.

   d. Con el documento legalizado, apostillado, traducido, validado y apostillado
nuevamente, vas a la municipalidad de la ciudad donde te queres registrar,
con tu pasaporte con visa de laburo y tramitas el BSN.

5. Se necesita el stamp del MVV en el pasaporte

Una vez que vuelve el papel con los v-numbers y esta todo aprobado se necesita el stamp (valido por 3 meses) en el pasaporte
esto se realiza en laEmbajada del Reino de los Países Bajos en Buenos Aires

Departamento Consular
Olga Cossettini 831, piso 3
C1107CDC Ciudad Autónoma de Buenos Aires

https://www.paisesbajosytu.nl/su-pais-y-los-paises-bajos/argentina/acerca-de-nosotros/embajada-en-buenos-aires


# Netherlands

1. Get the residence permit (ID NL)
  1.a ask/check with Michael/Machiel if it is ready your id. or you can call 088-0430-430, you can ask for an Appoinment to get it also
  1.b [Online Appoinment](https://ind.nl/en/Pages/Appointment-to-collect-your-residence-document.aspx)
  1.c Click in blue button at the end of the webpage
  1.d Select your location and schedule a date to pick up it.
2. Get the BSN number at the municipality
3. Open a bank account
4. Rent a house
5. Register in the city
  - My case I registered in  Rotterdam (https://rotterdamexpatcentre.nl/expats/formalities/changing-address/)
6. Register with a Doctor (GP)
  - https://rotterdamexpatcentre.nl/expats/health/general-practitioner/

#  Taxes

You will need to declare your taxes every year they will help you of course you must pay them
but it will save you money and time

* https://www.blueumbrella.nl/

#  Help

Este sitio contiene ayuda (abogados, tramites, etc) para las personas de habla hispana hasta tienen clases de idioma
http://www.nuestracasarotterdam.nl/es_ES/


# EMBAJADA DE LA REPUBLICA ARGENTINA / AMBASSADE VAN DE ARGENTIJNSE REPUBLIEK

```
Sección Consular / Consulaire Afdeling
Javastraat 20
2585 AN La Haya / Den Haag
Países Bajos / Nederland
Tel: +31 (0) 70-311 84 22/23/29
Fax: +31 (0) 70-311 84 10
E-mail: consular_epbaj@mrecic.gov.ar
www.epbaj.mrecic.gov.ar
```

# IND (Immigration and Naturalisation Service)

```
Tel: 088 0430 430

https://ind.nl/en/Pages/default.aspx

https://ind.nl/en/contact/Pages/Email.aspx
```
