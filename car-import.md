[Sharing] Self import car from Germany to NL
In this post, I’d like to share my personal experience self-importing car from Germany to Netherlands. The reason to import the car because there is huge selection of the models, very competitive price, higher specification, and some of dealers will offer warranty that also valid in EU including Netherlands. My recommendation is to get the used car from brand dealer that will always have service record history and always reputation to uphold

You can end up save at least 10 to 15% of the exact same car that is sold in Netherlands, and if you are importing an electric car (similar to my experience), there’s no BPM needed to be paid, so total cost of importing it would be for EUR150 only (RDW inspection + NL license plate) on top of the actual car price.

There are many sites offering services to import the car from Germany (they can only help you when you are buying from car dealer, not private individual). I think their fee is quite high considering the effort required. Therefore, below are the steps that you can follow, including the reference URL that you can use.



Before collecting the car in Germany:

1. Documents required from seller (https://easyimport.nl/auto-importeren/welke-duitse-autopapieren-nodig/)

Red license plate (Ausfuhrkennzeichen) 14-days validity under your name

Zulassungsbescheinigung teil 1 (fahrzeugschein) – under your name, issued with new Ausfuhrkennzeichen

Zulassungsbescheinigung teil 2 (fahrzeugbrief) – under your name, issued with new Ausfuhrkennzeichen

CoC (Certificate of Conformity) or EWG- Übereinstimmingsbescheinigung

Valid TÜV Certificate (APK Inspection paper) à recommended to ensure vehicle has passed inspection in Germany and certificate validity can be transferred to Netherlands

Green Card (certificate for liability insurance) under your name during validity of Ausfuhrkennzeichen

2. Request temporary 14-days exemption to drive foreign license plate in NL (https://www.belastingdienst.nl/wps/wcm/connect/bldcontenten/belastingdienst/individuals/cars/bpm/exemption/temporary-exemption-from-bpm-and-mrb/ )

Once you have the number of German Ausfuhrkennzeichen, apply via above form. No fee required and confirmation will be given straightaway after submitting all details. Print this confirmation and important to carry this when driving the car back to Netherlands.

Rightfully, using German Ausfuhrkennzeichen and exemption paper you may drive in Netherlands until the expiry date of the plate or the day of RDW inspection, whichever come first.

3. Make appointment for RDW Inspection (only VIN required to do booking, so you don't need to wait until you have the car) Cost is €127.55 (https://keuringsafspraakmaken.rdw.nl/particulier)

Ensure to schedule appointment as soon as possible before expiry of German Ausfuhrkennzeichen. The slots are filling fast.

4. Arrange NL 1 day plate after setting up appointment with RDW (valid only for the day of RDW Appointment) - https://eendagskentekenbewijsaanvragen.rdw.nl/particulier

Make the license plate using paper & cardboard box and ensure it’s waterproof (sealed using adhesive/packaging tape)

5. Request 1 day WA insurance (Autoverzekering voor eendagskenteken) - https://www.sluitsnel.nl/verzekering/eendagskenteken-verzekering/

Cost is EUR32.48 for 1 day validity during RDW inspection (you can’t run errands on that day using the vehicle, as it can only be driven to and from inspection place)



On the day of RDW inspection:

6. Attach RDW temporary license plate to the car and bring the printed copy of Eendagskentekenbewijs (from point 4) and insurance policy (from point 5)

7. Go to RDW and surrender the following documents (please make a copies before you handed over those documents for your archive):

Zulassungsbescheinigung teil 1

Zulassungsbescheinigung teil 2

CoC (Certificate of Conformity) or EWG- Übereinstimmingsbescheinigung

Valid ID with BSN on it (e.g. driving license)

8. BPM Declaration via Belastingdienst

The VIN will be registered in Belastingdienst website only after RDW inspection. But you need to do it straightaway after inspection to avoid delay on process. https://www.belastingdienst.nl/wps/wcm/connect/nl/auto-en-vervoer/content/online-aangifte-bpm

For this process, you will only need the net catalogue price of the car (when it’s new). I use the Koerslijst from ANWB (https://www.anwb.nl/auto/koerslijst/koerslijst)

RDW will only process the application and issue license plate once you have declared BPM and also settle the amount required to be paid.

Upon receiving registration certificate/card from RDW (it will be mailed to your address in less than 5 days):

9. Go to appointed workshop with to make number plate and bring along registration certificate from RDW. It cost EUR28 to make 2pcs of standard plates.

10. Arrange Car Insurance starting with the date of receiving registration certificate/plate.

Congratulations, you can now legally own and drive your car!!

reference:
https://www.reddit.com/r/Netherlands/comments/zrko3w/sharing_self_import_car_from_germany_to_nl/
Just to keep a local copy
